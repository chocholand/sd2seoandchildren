import java.util.*;
import java.io.*;
/**
 * @author kimyujin, kimjooho
 * @version 20131014
 * @param File I/O
 * @return 
 */

public class WordCounter {


	public static void main(String[] args) {

		ArrayList<String> arrayList=new ArrayList<String>();
		HashSet<String> hSet = new HashSet<String>();
		WordFrequencyCollection wordCol = new WordFrequencyCollection(arrayList,hSet);
		WordFrequencyAnalyzer wordAna = new WordFrequencyAnalyzer(wordCol);
	    
		try{
			wordAna.analyzeText(new FileReader("file.txt"));
		}
		catch(Exception e)
		{
			System.out.println("fail error");
		}
		
		wordCol = wordAna.getResults();
		for(String str: hSet) {
			wordCol.getFrequency(str);
			System.out.println(str +"   " +wordCol.count);
		}
		
		
		
	}
}
