<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>

<%	
	String mapNo = (String)request.getAttribute("mapNo");
	String diaryNo = (String)request.getAttribute("diaryNo");
	String title = (String)request.getAttribute("title");
	String contents = (String)request.getAttribute("contents");
	String date = (String)request.getAttribute("date");
		
	System.out.println("다이어리 넘버"+mapNo);
	System.out.println("diaryNo "+diaryNo);
	System.out.println("title" +title);
	System.out.println("contents "+contents);
	System.out.println("date "+date);
	
%>


<!doctype html>
<html>
<head>
<title>ROADSKETCH</title>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<link href="/roadsketch/assets/css/diary.css" rel="stylesheet" type="text/css">
	
<script type = "text/javascript"
 src = "http://maps.googleapis.com/maps/api/js?sensor=true">
</script>
</head>
<body onload="initialize()">
	<div id="container">
		<div id="header">
			<jsp:include page="/views/Include/header.jsp" flush="true" />
		</div>
		<div id="content">
			<div id="diary">	
				<div id="map_canvas" style="width:100%; height:300px"></div>
				 <div id="gmap_nav">
			 <ul id="gmap_list">
			 </ul>
			 </div>
			 <form action="/roadsketch/diary" method="post">
					<input type="hidden" name="a" value="edit">
					<input type="hidden" name="diaryNo" value=<%=diaryNo%>>
					<table border="1" cellpadding="5" cellspacing="0">
						<tr>
							<td colspan=1>제목</td><td colspan=3><input type="text" name="title" value=<%=title%>></td>
						</tr>
						<tr>
							<td colspan=4><textarea type="text" name="contents" cols=50 rows=5><%=contents%></textarea></td>
						</tr>
						<tr>
							<td colspan=4 align=right><input type="submit" VALUE=" 확인 "></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
		<div id="navigation">
			<jsp:include page="/views/Include/navigation.jsp" flush="true" />
		</div>
		<div id="footer">
			<p>(c)opyright 2013</p>
		</div>
	</div>
</body>
<script src="/roadsketch/assets/js/mapjs.js"></script>

</html>
