<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import=" kr.ac.smu.dao.*"%>
<%@ page import=" kr.ac.smu.vo.*"%>

<!doctype html>
<html>
<head>
<title>ROADSKETCH</title>

<meta http-equiv="content-type" content="text/html; charset=utf-8">
<link href="/roadsketch/assets/css/diary.css" rel="stylesheet"	type="text/css">
<script	type = "text/javascript" src = "/roadsketch/jquery/jquery-1.9.0.js" ></script>

<script>
	<% session = request.getSession( false );
		 UserVo voME = null;

		 if( session != null ){
		 	voME = (UserVo)session.getAttribute( "authUser" );
		 	System.out.println("sessions");
		 }
		 
		 //내 세션을 통해 이메일 주소를 가져온다.
		 String myEmail = voME.getEmail();
		 
		 DiaryDao DiaryDao = new DiaryDao();
		 ArrayList<DiaryVo> diarylist = DiaryDao.getList(myEmail);
		 
		 int diarySize = diarylist.size();
		 System.out.println("다이어리의 갯수:"+diarySize);
		 DiaryVo DiaryVo = new DiaryVo();		 
		 %>
</script>

</head>
<body>
	<div id="container">
		<div id="header">
			<jsp:include page="/views/Include/header.jsp" flush="true" />
		</div>
		<div id="content">
			<div id="diary">
				<table border="1" cellpadding="5" cellspacing="0">					
						<tr>
							<td>제목</td><td>날짜</td>						
						</tr>
						<tr>
						<%
							for (int i = 0; i <diarySize; i++) {
								 DiaryVo = diarylist.get(i);								
								 System.out.println("다이어리 목록 맵번호:"+DiaryVo.getMapNo());
						%>
							<tr>							
								<td><a href="/roadsketch/diary?a=view&no=<%=DiaryVo.getMapNo()%>"><%=DiaryVo.getTitle()%></a></td>								
							    <td><%=DiaryVo.getDate()%></td>
							</tr>						
						<%}%>						
					</table>			
				
			</div>
		</div>
		<div id="navigation">
			<jsp:include page="/views/Include/navigation.jsp" flush="true" />
		</div>
		<div id="footer">
			<p>(c)opyright 2013</p>
		</div>
	</div>
</body>
</html>