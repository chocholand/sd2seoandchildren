<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="kr.ac.smu.dao.*" %>
<%@ page import="kr.ac.smu.vo.*" %>
<%@ page import="kr.ac.smu.mvc.action.map.*" %>
<%@ page import="java.util.ArrayList" %>

<!doctype html>
<html>
<head>
<title>ROADSKETCH</title>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<link href="/roadsketch/assets/css/map.css" rel="stylesheet" type="text/css">

</head>
<body onload="initialize()">
	<div id="container">
		<div id="header">
			<jsp:include page="/views/Include/header.jsp" flush="true" />
		</div>

		<div id="content">
		<div id="map">
			<div id="map_canvas" style="width:100%; height:300px"></div>
  <p> <input type="button" id="clear" value="초기화" onclick='clearAll()'>
    <input type="button" id="viewAll" value="임시저장" onclick='viewAll()'>
    <input type="button" id="route" value="경로검색" onclick='calcRoute()'></p>
   
    <form method="post" action="/roadsketch/map" name ="save">
    	<input type="hidden" name="arr">
    	<input type="hidden" name="arr1">
    	<input type="hidden" name="arr2">
    	<input type ="hidden" name="subject">
    	<input type="button" value="저장하기" onclick='doSubmit()'>
    	</form>
    	
    	 <form method="post" action="/roadsketch/diary" name ="savetitle">
    	<input type="hidden" name="subject">
    	</form>
   
 <p> 주소     : <input type="text" size="50" id="addr1" name="address"/>  
            <input name="submit" type="submit" value="Search" onclick='codeAddress(); return false;' /> </p>
 <p> 메모 : <textarea name="comments" id="comment" rows="5" cols="50"></textarea>
                <input name="cSubmit" type="submit" value="입력" onclick='markerComments(); return false;' /> </p>



 <br>
 </div>
		</div>
		<div id="navigation">
			<jsp:include page="/views/Include/navigation.jsp" flush="true" />
		</div>
		<div id="footer">
			<p>(c)opyright 2013</p>
		</div>
	
</body>
<script type = "text/javascript" src = "http://maps.googleapis.com/maps/api/js?sensor=true"></script>



<script language="javascript" type="text/javascript">
 function loadMapFromDB(){
	 <% session = request.getSession( false );
	 UserVo voME = null;

	 if( session != null ){
	 	voME = (UserVo)session.getAttribute( "authUser" );
	 	System.out.println("sessions");
	 }

	 //내 세션을 통해 이메일 주소를 가져온다.
	 String myEmail = voME.getEmail();		

	 		MapDao mapDao = new MapDao();
	 		ArrayList<MapVo> maplist = mapDao.getList(myEmail); 		
	 		PointDao pointDao = new PointDao(); 	

	 		System.out.println("조회할 맵 번호:"+maplist.get(0).getMapNo());	 		
	 		ArrayList<PointVo> pointvolist = pointDao.getPointList(maplist.get(0)); 	
	 		int pSize = pointvolist.size();
	 		System.out.println("포인트의 갯수:"+pSize);
	 %>
	
	 function PointVos() {
			this.pLatitude;
			this.pLongtitude;
			this.pMessage;
		}
	 <% if(pSize > 0) { %>
var jaPV = new PointVos();

	 <% for(PointVo t : pointvolist) { %>
	
	 jaPV.pLatitude ="<%= t.getPointLatitude()%>";
	 jaPV.pLongtitude ="<%= t.getPointLongtitude()%>";
	 jaPV.pMessage ="<%= t.getMessage()%>";
	 codeCoordinate2(new google.maps.LatLng(jaPV.pLatitude, jaPV.pLongtitude),jaPV.pMessage);
	 <%}}%>

 }
</script>
<script src="/roadsketch/assets/js/mapjs.js"></script>

</html>