<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import=" java.util.List"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/roadsketch/assets/css/jquery.mobile-1.3.1.css" />
<link rel="stylesheet"	href="/roadsketch/assets/css/mobile.css" />
<script src="/roadsketch/assets/js/jquery/jquery-1.9.0.js"></script>
<script src="/roadsketch/assets/js/jquery/jquery.mobile-1.3.1.js"></script>

</head>
<body>

<div data-role="page">
	<div data-role="header">
			<h1>ROADSKETCH -지도</h1>
        </div>
		<div id="navigation">
			<jsp:include page="/views/Include/navigation_m.jsp" flush="true" >
			<jsp:param value="map" name="state"/>
			</jsp:include>
		</div>   
		
		

		</div>
		<div data-role="footer"><p>(c)opyright 2013</p></div>
	</div>
</body>
</html>