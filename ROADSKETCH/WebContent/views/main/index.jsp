<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<title>ROADSKETCH</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<link href="/roadsketch/assets/css/main.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="container">
		<div id="header">
		<jsp:include page ="/views/Include/header.jsp" flush="true"/>
		</div>
		<div id="wrapper">
			<div id="content">
				<div id="site-introduction">
				<img id="profile" src="/roadsketch/assets/image/road.jpg">
				
					
				</div>
			</div>
		</div>
		<div id="navigation">
			<jsp:include page ="/views/Include/navigation.jsp" flush="true"/>
		</div>
		<div id="footer">
			<p>(c)opyright 2013 </p>
		</div>
	</div>
</body>
</html>