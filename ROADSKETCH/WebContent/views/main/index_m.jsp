<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="kr.ac.smu.vo.UserVo"%>
<%
String result = (String) request.getParameter("result");
session = request.getSession(false);
UserVo vo = (UserVo)session.getAttribute("authUser");
%>

<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="/roadsketch/assets/css/jquery.mobile-1.3.1.css" />
<link rel="stylesheet" type="text/css" href="/roadsketch/assets/css/mobile.css" />
<script type="text/javascript" src="/roadsketch/assets/js/jquery/jquery-1.9.0.js"></script>
<script type="text/javascript" src="/roadsketch/assets/js/jquery/jquery.mobile-1.3.1.js"></script>

</head>
<body> 

<%
		if(vo==null){
	%>

    <div data-role="page" class="page-login"> 
        <div data-role="content">
        	<h1>ROADSKETCH</h1>
			<form id="login-form" data-ajax="false" name="loginform" method="post" action="/roadsketch/user?a=login&mobile=1">      	
			<div data-role="fieldcontain">
				<fieldset data-role="controlgroup">
					<input type="text" placeholder="이메일" name="email"  id="email" value="">
					<input type="text" placeholder="비밀번호" name="password"  id="password" value="">
					<%if ("fail".equals(result)){%>	
						<p class="error-text">
							이메일 , 비밀번호가 일치하지 않습니다.
						</p>
					<%}%>
					<input data-theme="b" id="submit" type="submit" value="로그인">	
				</fieldset>							
			</div>
			</form>
			
			<p class="join-text">
				아직 회원이 아니세요? <a href="">회원가입</a>
			</p>
        </div> 
    </div>
    
    <%
		}else{
	%>	
	   <div data-role="page"> 
        <div data-role="header">
			<h1>ROADSKETCH</h1>
			<a href="/roadsketch/user?a=logout&mobile=1" data-role="button" data-inline="true" class="ui-btn-right">로그아웃</a>
        </div>
		<div id="navigation">
			<jsp:include page="/views/Include/navigation_m.jsp" flush="true" >
			<jsp:param value="main" name="state"/>
			</jsp:include>
		</div>       
        <div data-role="content">       
			<div class="welcome-message">
				
				<br><br>				
			</div>
        </div> 
        <div data-role="footer"><p>(c)opyright 2013</p></div>
    </div> 
    <%
		}
	%> 
     
</body>
</html>