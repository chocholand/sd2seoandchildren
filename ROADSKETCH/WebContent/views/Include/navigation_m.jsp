<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ page import="kr.ac.smu.vo.UserVo"%>
<%
	String state = request.getParameter("state");
%>

<div data-role="navbar" data-theme="a">
	<ul>
	<%if(state.equals("main")){ %>
	<li><a href="/roadsketch/views/main/index_m.jsp" class="ui-btn-active">로드스케치</a></li>
	<li><a href="/roadsketch/views/map/list_m.jsp">지도</a></li>
	<li><a href="/roadsketch/views/diary/contentList_m.jsp">다이어리</a></li>	
	
	<%}else if(state.equals("guestbook")){ %>
	<li><a href="/roadsketch/views/main/index_m.jsp" >로드스케치</a></li>
	<li><a href="/roadsketch/views/map/list_m.jsp" class="ui-btn-active">지도</a></li>
	<li><a href="/roadsketch/views/diary/contentList_m.jsp">다이어리</a></li>	
	
	<%}else if(state.equals("board")){ %>
	<li><a href="/roadsketch/views/main/index_m.jsp" >로드스케치</a></li>
	<li><a href="/roadsketch/views/map/list_m.jsp">지도</a></li>
	<li><a href="/roadsketch/views/diary/contentList_m.jsp" class="ui-btn-active">다이어리</a></li>		
	<%}%>
	</ul>
</div>  