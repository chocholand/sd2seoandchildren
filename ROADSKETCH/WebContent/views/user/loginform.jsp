<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	//String result = (String) request.getAttribute("result");
      String result = (String) request.getParameter("result");
%>

<!doctype html>
<html>
<head>
<title>ROADSKETCH</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<link href="/roadsketch/assets/css/user.css" rel="stylesheet"
	type="text/css">
</head>
<body>
	<div id="container">
		<div id="header">
			<jsp:include page="/views/Include/header.jsp" flush="true" />
		</div>
		<div id="content">
			<div id="user">

				<form id="login-form" name="loginform" method="post"
					action="/roadsketch/user">
					<input type="hidden" name="a" value="login">
					<label class="block-label" for="email">이메일</label> <input
						id="email" name="email" type="text" value=""> <label
						class="block-label">패스워드</label> <input name="password"
						type="password" value=""> <%if ("fail".equals(result)) {
						%>
					<p>로그인이 실패 했습니다.</p>

					<% }%> <input type="submit" value="로그인">
				</form>
			</div>
		</div>
		<div id="navigation">
			<jsp:include page ="/views/Include/navigation.jsp" flush="true"/>
		</div>
		<div id="footer">
			<p>(c)opyright 2013</p>
		</div>
	</div>
</body>
</html>