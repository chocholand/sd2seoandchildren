<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<title>ROADSKETCH</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<link href="/roadsketch/assets/css/user.css" rel="stylesheet"
	type="text/css">
<script	type = "text/javascript" src = "/roadsketch/jquery/jquery-1.9.0.js" ></script>

<script>
	$(function() {
		$("#join-form input[type='button']").click(function() {
			var $email = $("#email");
			var email = $email.val();
			if (email == "") {
				alert("이메일이 비어 있습니다.");
				return;
			}
			$.ajax({
				async: true,
				url : "/roadsketch/user?a=quser&email="+email,
				type : "get",
				dataType : "json",
				data : "",
				contentType :'application/json',
				success : function(data) {
					
					if(data.result =="exist"){
						alert("존재하는 이메일 입니다. 다시입력해 주세요");
					}else{
						alert("사용할 수 있는 이메일입니다.");
					}
				},
				error : function(jqXHR, status, e) {
					alert(status + " : " + e);
				}
			});

		});
	});
</script>
</head>
<body>
	<div id="container">
		<div id="header">
			<jsp:include page="/views/Include/header.jsp" flush="true" />
		</div>
		<div id="content">
			<div id="user">

				<form id="join-form" name="joinForm" method="post"
					action="/roadsketch/user">
					<input type="hidden" name="a" value="join"> <label
						class="block-label" for="name">이름</label> <input id="name"
						name="name" type="text" value=""> <label
						class="block-label" for="email">이메일</label> <input id="email"
						name="email" type="text" value=""> <input type="button"
						value="id 중복체크"> <label class="block-label">패스워드</label> <input
						name="password" type="password" value="">

					<fieldset>
						<legend>성별</legend>
						<label>여</label> <input type="radio" name="gender" value="female"
							checked="checked"> <label>남</label> <input type="radio"
							name="gender" value="male">
					</fieldset>

					<fieldset>
						<legend>약관동의</legend>
						<input id="agree-prov" type="checkbox" name="agreeProv" value="y">
						<label>서비스 약관에 동의합니다.</label>
					</fieldset>

					<input type="submit" value="가입하기">

				</form>
			</div>
		</div>
		<div id="navigation">
			<jsp:include page="/views/Include/navigation.jsp" flush="true" />
		</div>
		<div id="footer">
			<p>(c)opyright 2013</p>
		</div>
	</div>
</body>
</html>