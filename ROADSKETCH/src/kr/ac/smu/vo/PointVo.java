package kr.ac.smu.vo;

public class PointVo {
	
	private int pointNo;
	private String date;
	private String message;	
	private int mapNo;	
	private String pointLatitude;  
	private String pointLongtitude;
	
	public int getPointNo() {
		return pointNo;
	}
	public void setPointNo(int pointNo) {
		this.pointNo = pointNo;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getMapNo() {
		return mapNo;
	}
	public void setMapNo(int mapNo) {
		this.mapNo = mapNo;
	}
	public String getPointLatitude() {
		return pointLatitude;
	}
	public void setPointLatitude(String pointLatitude) {
		this.pointLatitude = pointLatitude;
	}
	public String getPointLongtitude() {
		return pointLongtitude;
	}
	public void setPointLongtitude(String pointLongtitude) {
		this.pointLongtitude = pointLongtitude;
	}  	
	
}
