package kr.ac.smu.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.action.main.IndexAction;


public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String a = request.getParameter("a");
		Action action = null;
		if("index".equals(a)){
			action =new IndexAction(); //오브젝트 형은 부모 뉴는 자식, 함수를감추기 위해서.			
		}else{
			action = new IndexAction();	
		}
		action.execute(request, response);		
	}
}