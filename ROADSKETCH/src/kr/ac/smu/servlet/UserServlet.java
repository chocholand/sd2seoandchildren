package kr.ac.smu.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;
import kr.ac.smu.mvc.action.user.JoinAction;
import kr.ac.smu.mvc.action.user.JoinSuccessAction;
import kr.ac.smu.mvc.action.user.JoinformAction;
import kr.ac.smu.mvc.action.user.LoginAction;
import kr.ac.smu.mvc.action.user.LoginFormAction;
import kr.ac.smu.mvc.action.user.LogoutAction;
import kr.ac.smu.mvc.action.user.QueryUserAction;

public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String a = request.getParameter("a");
		Action action = null;
		if("joinform".equals(a)){
			action= new JoinformAction();			
		}else if("join".equals(a)){
			action= new JoinAction();	
		}
		else if("joinsuccess".equals(a)){
			action= new JoinSuccessAction();	
		}
		else if("loginform".equals(a)){
			action= new LoginFormAction();	
		}
		else if("login".equals(a)){
			action= new LoginAction();
		}
		else if("logout".equals(a)){
			action= new LogoutAction();
		}	
		else if("quser".equals(a)){
			action= new QueryUserAction();
		}	
		else{
			String mobile= request.getParameter("mobile");
			request.setAttribute("mobile",mobile);
			HttpUtil.redirect(request, response, "/roadsketch/main");//리다이렉트 했다고 끝났다고 생각하면 안된다.
			return; // 아래로 내려가지 않게 리턴해준다.
		}
		action.execute(request, response);
	}

}
