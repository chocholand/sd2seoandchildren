package kr.ac.smu.servlet;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;
import kr.ac.smu.mvc.action.diary.EditAction;
import kr.ac.smu.mvc.action.diary.ViewAction;
import kr.ac.smu.mvc.action.map.DiaryAction;

public class DiaryServlet  extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request , response);
		}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//한글처리
		request.setCharacterEncoding("UTF-8");
		
		String a =request.getParameter("a");
		Action action = null;
		//액션을 뽑아내는 코드
		if("diarySave".equals(a)){
			action = new DiaryAction();			
		}	
		else if("view".equals(a)){
			action = new ViewAction();			
		}
		else if("edit".equals(a)){
			action = new EditAction();			
		}
		else{
			//뷰를 띄울때 포워딩을 사용한다.			
			HttpUtil.forward(request, response, "/views/diary/contentList.jsp");
			return; // 아래로 내려가지 않게 리턴해준다.
		}
		action.execute(request, response);	
	}
}
