package kr.ac.smu.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import kr.ac.smu.vo.MapVo;
import kr.ac.smu.vo.PointVo;

public class PointDao {

	public void insert(PointVo vo){

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver"); 			
			String dburl ="jdbc:oracle:thin:@127.0.0.1:1521:xe";			
			conn = DriverManager.getConnection(dburl,"dev","dev");			
			
			String sql ="insert into point values(point_no_seq.nextval,SYSDATE,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);				
			pstmt.setString(1,vo.getMessage());
			pstmt.setInt(2,vo.getMapNo());	
			pstmt.setString(3,vo.getPointLatitude());	
			pstmt.setString(4,vo.getPointLongtitude());	
			pstmt.executeUpdate();
			
			System.out.println("insert-OK(Point)");
			conn.commit();

		} catch (ClassNotFoundException e){			
			e.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt !=null){
					pstmt.close();}
			}catch(SQLException ex){}

			try{
				if(conn !=null){
					conn.close();
				}				
			}catch(SQLException ex){}			
		}

	}
	
	//조회하고자 하는 맵의 포인트 정보 가져오기(map_no)
	public ArrayList<PointVo> getPointListNo(int map_no){
		ArrayList<PointVo> list = new ArrayList<PointVo>(); 		
		Connection conn = null;
		PreparedStatement pstmt = null;

		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			String dburl ="jdbc:oracle:thin:@127.0.0.1:1521:xe";
			conn = DriverManager.getConnection(dburl,"dev","dev");			
		
			String sql = 
					  "select p.point_no, to_char(p.regdate, 'yyyy-mm-dd am hh:mi:ss'), p.message, p.point_latitude, p.point_longtitude"
					+ " from point p where map_no=?";
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, map_no);
			ResultSet rs = pstmt.executeQuery();

			while( rs.next() ){
				PointVo vo2 = new PointVo();
				vo2.setPointNo(rs.getInt(1));
				vo2.setDate(rs.getString(2));
				vo2.setMessage(rs.getString(3));
				vo2.setPointLatitude(rs.getString(4));
				vo2.setPointLongtitude(rs.getString(5));
				list.add(vo2);
			}
			rs.close();
			System.out.println("getPointList-ok");

		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt != null)
					pstmt.close();
			}catch(SQLException ex){}

			try {
				if(conn != null)
					conn.close();
			}catch (SQLException e) {}
		}

		return list;
	}
	
	
	
	
	
	
		
	
	//조회하고자 하는 맵의 포인트 정보 가져오기
		public ArrayList<PointVo> getPointList(MapVo vo){
			ArrayList<PointVo> list = new ArrayList<PointVo>(); 		
			Connection conn = null;
			PreparedStatement pstmt = null;

			try{
				Class.forName("oracle.jdbc.driver.OracleDriver");
				String dburl ="jdbc:oracle:thin:@127.0.0.1:1521:xe";
				conn = DriverManager.getConnection(dburl,"dev","dev");			
			
				String sql = 
						  "select p.point_no, to_char(p.regdate, 'yyyy-mm-dd am hh:mi:ss'), p.message, p.point_latitude, p.point_longtitude"
						+ " from point p where map_no=?";
				
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, vo.getMapNo());
				ResultSet rs = pstmt.executeQuery();

				while( rs.next() ){
					PointVo vo2 = new PointVo();
					vo2.setPointNo(rs.getInt(1));
					vo2.setDate(rs.getString(2));
					vo2.setMessage(rs.getString(3));
					vo2.setPointLatitude(rs.getString(4));
					vo2.setPointLongtitude(rs.getString(5));
					list.add(vo2);
				}
				rs.close();
				System.out.println("getPointList-ok");

			}catch(ClassNotFoundException ex){
				ex.printStackTrace();
			}catch(SQLException ex){
				ex.printStackTrace();
			}finally{
				try{
					if(pstmt != null)
						pstmt.close();
				}catch(SQLException ex){}

				try {
					if(conn != null)
						conn.close();
				}catch (SQLException e) {}
			}

			return list;
		}
	
	
	//사용자가 가지고있는 모든 맵의 맵별 포인트 정보 가져오기
	public ArrayList<PointVo> getList(MapVo vo){
		ArrayList<PointVo> list = new ArrayList<PointVo>(); 		
		Connection conn = null;
		PreparedStatement pstmt = null;

		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			String dburl ="jdbc:oracle:thin:@127.0.0.1:1521:xe";
			conn = DriverManager.getConnection(dburl,"dev","dev");			
		
			String sql = 
					  "select p.point_no, to_char(p.regdate, 'yyyy-mm-dd am hh:mi:ss'), p.message, p.point_latitude, p.point_longtitude"
					+ " from point p, (select map_no as mn"
					+ " from map where email = ?) m1"
					+ " where m1.mn = p.map_no order by point_no";
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, vo.getEmail());
			ResultSet rs = pstmt.executeQuery();

			while( rs.next() ){
				PointVo vo2 = new PointVo();
				vo2.setPointNo(rs.getInt(1));
				vo2.setDate(rs.getString(2));
				vo2.setMessage(rs.getString(3));
				vo2.setPointLatitude(rs.getString(4));
				vo2.setPointLongtitude(rs.getString(5));
				list.add(vo2);
			}
			rs.close();
			System.out.println("getList-ok");

		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt != null)
					pstmt.close();
			}catch(SQLException ex){}

			try {
				if(conn != null)
					conn.close();
			}catch (SQLException e) {}
		}

		return list;
	}
	
	
	
	//맵번호에 해당하는 모든 포인터 지우기(맵초기화)
		public void deleteAllPoint( MapVo vo ){
			Connection conn = null;
			PreparedStatement pstmt = null;

			try{

				Class.forName("oracle.jdbc.driver.OracleDriver");
				String dburl ="jdbc:oracle:thin:@127.0.0.1:1521:xe";
				conn = DriverManager.getConnection(dburl,"dev","dev");	
							
				String sql = "delete from point where map_no  = ?";
				pstmt = conn.prepareStatement(sql); 
				pstmt.setInt(1, vo.getMapNo());				
				pstmt.executeUpdate();
				
				System.out.println("deleteAllPoint-OK");
				conn.commit();

			}catch(ClassNotFoundException ex){
				ex.printStackTrace();
			}catch(SQLException ex){
				ex.printStackTrace();
			}finally{
				try{
					if(pstmt != null)
						pstmt.close();
				}catch(SQLException ex){}

				try {
					if(conn != null)
						conn.close();
				}catch (SQLException e) {}
			}
		}
		
		
		//포인터 지우기(포인트 삭제)
		public void deletePoint( PointVo vo ){
			Connection conn = null;
			PreparedStatement pstmt = null;

			try{

				Class.forName("oracle.jdbc.driver.OracleDriver");
				String dburl ="jdbc:oracle:thin:@127.0.0.1:1521:xe";
				conn = DriverManager.getConnection(dburl,"dev","dev");	
							
				String sql = "delete from point where point_no = ?";
				pstmt = conn.prepareStatement(sql); 
				pstmt.setInt(1, vo.getPointNo());				
				pstmt.executeUpdate();
				
				System.out.println("deletePoint-OK");
				conn.commit();

			}catch(ClassNotFoundException ex){
				ex.printStackTrace();
			}catch(SQLException ex){
				ex.printStackTrace();
			}finally{
				try{
					if(pstmt != null)
						pstmt.close();
				}catch(SQLException ex){}

				try {
					if(conn != null)
						conn.close();
				}catch (SQLException e) {}
			}
		}
	
}
