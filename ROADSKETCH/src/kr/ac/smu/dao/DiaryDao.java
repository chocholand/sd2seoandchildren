package kr.ac.smu.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import kr.ac.smu.vo.DiaryVo;
import kr.ac.smu.vo.MapVo;

public class DiaryDao {

	// 맵별 다이어리 정보 가져오기
	public ArrayList<DiaryVo> getList(String email){
		ArrayList<DiaryVo> list = new ArrayList<DiaryVo>(); 		
		Connection conn = null;
		PreparedStatement pstmt = null;

		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			String dburl ="jdbc:oracle:thin:@127.0.0.1:1521:xe";
			conn = DriverManager.getConnection(dburl,"dev","dev");			

			String sql = 
					"select d.diary_no, to_char(d.regdate, 'yyyy-mm-dd am hh:mi:ss'), d.title, d.contents, d.map_no"
							+ " from diary d, (select map_no as mn"
							+ " from map where email = ?) m1"
							+ " where m1.mn = d.map_no order by diary_no desc";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			ResultSet rs = pstmt.executeQuery();

			while( rs.next() ){
				DiaryVo vo2 = new DiaryVo();
				vo2.setDiaryNo(rs.getInt(1));
				vo2.setDate(rs.getString(2));
				vo2.setTitle(rs.getString(3));
				vo2.setContents(rs.getString(4));
				vo2.setMapNo(rs.getInt(5));
				list.add(vo2);
			}
			rs.close();
			System.out.println("getList-ok");

		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt != null)
					pstmt.close();
			}catch(SQLException ex){}

			try {
				if(conn != null)
					conn.close();
			}catch (SQLException e) {}
		}

		return list;
	}



	// 맵별 다이어리 정보 가져오기(map_no)
	public DiaryVo getListNo(String map_no){
		ArrayList<DiaryVo> list = new ArrayList<DiaryVo>(); 		
		Connection conn = null;
		PreparedStatement pstmt = null;
		DiaryVo vo= null; 
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			String dburl ="jdbc:oracle:thin:@127.0.0.1:1521:xe";
			conn = DriverManager.getConnection(dburl,"dev","dev");			

			String sql = 
					"select diary_no, to_char(regdate, 'yyyy-mm-dd am hh:mi:ss'), title, contents"
							+ " from diary where map_no= ?";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, map_no);
			ResultSet rs = pstmt.executeQuery();

			while( rs.next() ){
				vo = new DiaryVo();
				vo.setDiaryNo(rs.getInt(1));
				vo.setDate(rs.getString(2));
				vo.setTitle(rs.getString(3));
				vo.setContents(rs.getString(4));
			}
			rs.close();
			System.out.println("getList-ok");

		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt != null)
					pstmt.close();
			}catch(SQLException ex){}

			try {
				if(conn != null)
					conn.close();
			}catch (SQLException e) {}
		}

		return vo;
	}



	//다이어리 생성
	public void insert(DiaryVo vo){

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {

			Class.forName("oracle.jdbc.driver.OracleDriver"); 			
			String dburl ="jdbc:oracle:thin:@127.0.0.1:1521:xe";			
			conn = DriverManager.getConnection(dburl,"dev","dev");			
			String sql ="insert into diary values(diary_no_seq.nextval,?,?,SYSDATE,?)";

			pstmt = conn.prepareStatement(sql);					
			pstmt.setString(1,vo.getTitle());
			pstmt.setString(2,vo.getContents());
			pstmt.setInt(3, vo.getMapNo());
			pstmt.executeUpdate();

			System.out.println("insert-OK(Diary)");
			conn.commit();

		} catch (ClassNotFoundException e){			
			e.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt !=null){
					pstmt.close();}
			}catch(SQLException ex){}

			try{
				if(conn !=null){
					conn.close();
				}				
			}catch(SQLException ex){}			
		}

	}
	
	
	
	public void update(DiaryVo vo){

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");	
			String dburl ="jdbc:oracle:thin:@127.0.0.1:1521:xe";			
			conn = DriverManager.getConnection(dburl,"dev","dev");
			String sql ="update diary set title=? ,contents=? where diary_no =?";
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1,vo.getTitle());
			pstmt.setString(2,vo.getContents());
			pstmt.setInt(3,vo.getDiaryNo());			
				
			pstmt.executeUpdate();

			System.out.println("ok -update");

		} catch (ClassNotFoundException e){			
			e.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt !=null){
					pstmt.close();}
			}catch(SQLException ex){}

			try{
				if(conn !=null){
					conn.close();
				}				
			}catch(SQLException ex){}			
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
