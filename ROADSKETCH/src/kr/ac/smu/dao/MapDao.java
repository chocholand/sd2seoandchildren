package kr.ac.smu.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import kr.ac.smu.vo.MapVo;
import kr.ac.smu.vo.PointVo;

public class MapDao {

	public void insert(MapVo vo){

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver"); 			
			String dburl ="jdbc:oracle:thin:@127.0.0.1:1521:xe";			
			conn = DriverManager.getConnection(dburl,"dev","dev");			
			String sql ="insert into map values(map_no_seq.nextval,?)";
			pstmt = conn.prepareStatement(sql);			
			pstmt.setString(1,vo.getEmail());				
			pstmt.executeUpdate();

			System.out.println("insert-OK(Map)");
			conn.commit();

		} catch (ClassNotFoundException e){			
			e.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt !=null){
					pstmt.close();}
			}catch(SQLException ex){}

			try{
				if(conn !=null){
					conn.close();
				}				
			}catch(SQLException ex){}			
		}

	}


	//맵에서 이메일에 해당하는 맵 번호 리스트 가져오기
	public ArrayList<MapVo> getList(String email){
		ArrayList<MapVo> list = new ArrayList<MapVo>(); 		
		Connection conn = null;
		PreparedStatement pstmt = null;

		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			String dburl ="jdbc:oracle:thin:@127.0.0.1:1521:xe";
			conn = DriverManager.getConnection(dburl,"dev","dev");		
			String sql = "select map_no from map where email = ? order by map_no desc";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			ResultSet rs = pstmt.executeQuery();

			while( rs.next() ){
				MapVo vo2 = new MapVo();
				vo2.setMapNo(rs.getInt(1));
				list.add(vo2);
			}
			rs.close();
			System.out.println("getListMapNo-ok");

		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt != null)
					pstmt.close();
			}catch(SQLException ex){}

			try {
				if(conn != null)
					conn.close();
			}catch (SQLException e) {}
		}

		return list;
	}


}
