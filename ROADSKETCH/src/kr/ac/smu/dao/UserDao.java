package kr.ac.smu.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import kr.ac.smu.vo.UserVo;

public class UserDao {

	//이메일 중복체크
	public UserVo getUser(String email){
		UserVo vo = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {			
			Class.forName("oracle.jdbc.driver.OracleDriver"); 


			String dburl ="jdbc:oracle:thin:@127.0.0.1:1521:xe";			
			conn = DriverManager.getConnection(dburl,"dev","dev");		


			String sql ="select *from member where email=? ";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,email) ;					
			pstmt.executeQuery();
			ResultSet rs = pstmt.executeQuery();

			if(rs.next()){
				vo = new UserVo();
				vo.setEmail(rs.getString(1));
				vo.setName(rs.getString(2));							
			}



		} catch (ClassNotFoundException e){			
			e.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt !=null){
					pstmt.close();}
			}catch(SQLException ex){}

			try{
				if(conn !=null){
					conn.close();
				}				
			}catch(SQLException ex){}			
		}
		return vo;

	}

	public UserVo getUser(String email, String password){
		UserVo vo = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {

			Class.forName("oracle.jdbc.driver.OracleDriver"); 


			String dburl ="jdbc:oracle:thin:@127.0.0.1:1521:xe";			
			conn = DriverManager.getConnection(dburl,"dev","dev");		


			String sql ="select *from member where email=? and password=?";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,email) ;
			pstmt.setString(2, password) ;


			pstmt.executeQuery();

			ResultSet rs = pstmt.executeQuery();

			if(rs.next()){
				vo = new UserVo();
				vo.setEmail(rs.getString(1));
				vo.setName(rs.getString(2));				
			}

		} catch (ClassNotFoundException e){			
			e.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt !=null){
					pstmt.close();}
			}catch(SQLException ex){}

			try{
				if(conn !=null){
					conn.close();
				}				
			}catch(SQLException ex){}			
		}
		return vo;

	}

	public void insert(UserVo vo){

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {

			Class.forName("oracle.jdbc.driver.OracleDriver"); 			
			String dburl ="jdbc:oracle:thin:@127.0.0.1:1521:xe";			
			conn = DriverManager.getConnection(dburl,"dev","dev");			
			String sql ="insert into member values(?,?,?,?)";
			
			pstmt = conn.prepareStatement(sql);					
			pstmt.setString(1,vo.getEmail()) ;
			pstmt.setString(2,vo.getName()) ;
			pstmt.setString(3,vo.getPassword());
			pstmt.setString(4,vo.getGender());					
			pstmt.executeUpdate();

		} catch (ClassNotFoundException e){			
			e.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt !=null){
					pstmt.close();}
			}catch(SQLException ex){}

			try{
				if(conn !=null){
					conn.close();
				}				
			}catch(SQLException ex){}			
		}

	}
}
