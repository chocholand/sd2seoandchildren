package kr.ac.smu.mvc.action.map;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.ac.smu.dao.MapDao;
import kr.ac.smu.dao.PointDao;
import kr.ac.smu.dao.UserDao;
import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;
import kr.ac.smu.vo.MapVo;
import kr.ac.smu.vo.PointVo;
import kr.ac.smu.vo.UserVo;

import java.util.ArrayList;
import java.util.Calendar;;

public class MapAction extends Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		/***********************************************************/
		HttpSession session = request.getSession( false );
		UserVo voME = null;

		if( session != null ){
			voME = (UserVo)session.getAttribute( "authUser" );
			System.out.println("session");
		}


		//새로 맵 세팅하기		
		System.out.println("OK");
		String [] longtitude = request.getParameter("arr").split(",");//경도 longtitude
		String [] latitude = request.getParameter("arr1").split(",");//위도 latitude
		String [] message = request.getParameter("arr2").split(",");//메모
		
		System.out.println("받은 배열의 길이:"+longtitude.length);

		//내 세션을 통해 이메일 주소를 가져온다.
		String myEmail = voME.getEmail();		
	
		//여기부분 수정해야함 현재 이메일의 해당되는 map no 들중에 가장 큰 숫자의 no를 가져온다. 
		//디비 가져올때 내림차순 정렬해서 처음것만 가져다 쓰는걸로 하자.
		ArrayList<MapVo> list = new ArrayList<MapVo>();
		MapDao mapDao = new MapDao();		
		list = mapDao.getList(myEmail);
		int mapNo = list.get(0).getMapNo();		
		
		System.out.println("맵 번호:"+mapNo);

		PointDao pointDao = new PointDao();
		PointVo point = new PointVo();


		for(int i=0; i<latitude.length ;i++){

			point.setPointLatitude(latitude[i]);
			point.setPointLongtitude(longtitude[i]);
			point.setMessage(message[i]);			
			//맵 번호 세팅
			point.setMapNo(mapNo);

			//포인트 마다 DB에 저장한다.
			//포인트(경도,위도,날짜,메세지,맵 넘버)를 저장한다.
			pointDao.insert(point);

			System.out.println(latitude[i]);
			System.out.println(longtitude[i]);
			System.out.println(message[i]);
		}
		
		HttpUtil.forward(request, response, "/views/main/index.jsp");
		
	}

}


