package kr.ac.smu.mvc.action.map;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.ac.smu.dao.MapDao;
import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;
import kr.ac.smu.vo.DiaryVo;
import kr.ac.smu.vo.MapVo;
import kr.ac.smu.vo.UserVo;
import kr.ac.smu.dao.DiaryDao;

import java.util.ArrayList;

public class DiaryAction extends Action{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		/***********************************************************/
		HttpSession session = request.getSession( false );
		UserVo voME = null;

		if( session != null ){
			voME = (UserVo)session.getAttribute( "authUser" );
			System.out.println("session");
		}

		String diaryComments = request.getParameter("DiaryMessage");
		String title = request.getParameter("subject");

		String myEmail = voME.getEmail();	

		ArrayList<MapVo> list = new ArrayList<MapVo>();
		MapDao mapDao = new MapDao();		
		list = mapDao.getList(myEmail);		
		int mapNo = list.get(0).getMapNo();			

		DiaryDao diaryDao = new DiaryDao();
		DiaryVo diaryVo = new DiaryVo();

		diaryVo.setContents(diaryComments);
		diaryVo.setMapNo(mapNo);
		diaryVo.setTitle(title);

		//다이어리 생성
		diaryDao.insert(diaryVo);

		//다이어리가 생성되면 새로운 맵이 추가된다.
		//맵 추가
		MapVo mapVo = new MapVo();
		mapVo.setEmail(myEmail);			
		mapDao.insert(mapVo);

		String mobile= request.getParameter("mobile");
		request.setAttribute("mobile",mobile);
		HttpUtil.forward(request, response, "/views/main/index.jsp");
		//HttpUtil.forward(request, response, "/views/diary/contentList.jsp");

	}

}
