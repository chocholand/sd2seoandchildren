package kr.ac.smu.mvc.action.map;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.ac.smu.dao.MapDao;
import kr.ac.smu.dao.PointDao;
import kr.ac.smu.dao.UserDao;
import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;
import kr.ac.smu.vo.MapVo;
import kr.ac.smu.vo.PointVo;
import kr.ac.smu.vo.UserVo;

import java.util.ArrayList;
import java.util.Calendar;;

public class deleteMapAction extends Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		/***********************************************************/
		HttpSession session = request.getSession( false );
		UserVo voME = null;

		if( session != null ){
			voME = (UserVo)session.getAttribute( "authUser" );
			System.out.println("session");
		}

		//맵초기화 하는 곳
		String myEmail = voME.getEmail();		
		ArrayList<MapVo> list = new ArrayList<MapVo>();
		MapDao mapDao = new MapDao();	
	
		list = mapDao.getList(myEmail);
			
		PointDao pointDao = new PointDao();
		pointDao.deleteAllPoint(list.get(0));
		

		HttpUtil.forward(request, response, "/views/main/list.jsp");
		
	}

}


