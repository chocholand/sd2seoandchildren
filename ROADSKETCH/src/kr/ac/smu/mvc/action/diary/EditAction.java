package kr.ac.smu.mvc.action.diary;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.ac.smu.dao.DiaryDao;
import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;
import kr.ac.smu.vo.DiaryVo;
import kr.ac.smu.vo.UserVo;

public class EditAction extends Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		/***********************************************************/
		HttpSession session = request.getSession( false );
		UserVo voME = null;

		if( session != null ){
			voME = (UserVo)session.getAttribute( "authUser" );
			System.out.println("session");
		}
		
		String diaryNo = request.getParameter("diaryNo");
		String title = request.getParameter("title");
		String contents = request.getParameter("contents");
		
		System.out.println("다이어리 넘버"+diaryNo);
		System.out.println(title);
		System.out.println(contents);
		

		DiaryVo diaryVo = new DiaryVo();
		diaryVo.setDiaryNo(Integer.parseInt(diaryNo));
		diaryVo.setTitle(title);
		diaryVo.setContents(contents);
		
		DiaryDao diaryDao = new DiaryDao();
		diaryDao.update(diaryVo);
		
		
		HttpUtil.forward(request, response, "/views/diary/contentList.jsp");

		
	}

}
