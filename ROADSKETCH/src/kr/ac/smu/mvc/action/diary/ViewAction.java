package kr.ac.smu.mvc.action.diary;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.ac.smu.dao.DiaryDao;
import kr.ac.smu.dao.PointDao;
import kr.ac.smu.dao.UserDao;
import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;
import kr.ac.smu.vo.DiaryVo;
import kr.ac.smu.vo.PointVo;
import kr.ac.smu.vo.UserVo;

public class ViewAction extends Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		/***********************************************************/
		HttpSession session = request.getSession( false );
		UserVo voME = null;

		if( session != null ){
			voME = (UserVo)session.getAttribute( "authUser" );
			System.out.println("session");
		}
		//한글처리
		request.setCharacterEncoding("UTF-8");	
		String no= request.getParameter("no"); 
				
		System.out.println("넘어온 no 값:"+no);
		
		//포인트 정보		
		DiaryDao diaryDao = new DiaryDao();
		DiaryVo DiaryVo = diaryDao.getListNo(no);	    
				
		//다이어리 정보			
		String diaryNo = Integer.toString(DiaryVo.getDiaryNo());
		String title= DiaryVo.getTitle();
		String contents =DiaryVo.getContents();
		String date= DiaryVo.getDate();
		
		System.out.println("다이어리test:"+diaryNo);
		System.out.println(title);
		System.out.println(contents);
		System.out.println(date);
		
		request.setAttribute("mapNo",no);
		request.setAttribute("diaryNo",diaryNo);
		request.setAttribute("title",title);
		request.setAttribute("contents",contents);
		request.setAttribute("date",date);		

		HttpUtil.forward(request, response, "/views/diary/contentListview.jsp");

	}

}
