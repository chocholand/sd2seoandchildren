package kr.ac.smu.mvc.action.user;
//유저의 액션을 모아놓은 패키지 이다.

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.ac.smu.dao.MapDao;
import kr.ac.smu.dao.UserDao;
import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;
import kr.ac.smu.vo.MapVo;
import kr.ac.smu.vo.UserVo;

public class JoinAction extends Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String gender = request.getParameter("gender");
		String mobile= request.getParameter("mobile");

		System.out.println(name);
		System.out.println(email);
		System.out.println(password);
		System.out.println(gender);

		UserVo vo =new UserVo();
		vo.setName(name);
		vo.setEmail(email);
		vo.setPassword(password);;
		vo.setGender(gender);

		UserDao dao = new UserDao();
		dao.insert(vo);
		
		//맵 추가
		MapDao mapDao = new MapDao();
		MapVo mapVo = new MapVo();
		mapVo.setEmail(vo.getEmail());			
		mapDao.insert(mapVo);

		request.setAttribute("mobile",mobile);
		HttpUtil.redirect(request, response, "/roadsketch/user?a=joinsuccess");
	}


}
