package kr.ac.smu.mvc.action.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.ac.smu.dao.UserDao;
import kr.ac.smu.mvc.Action;
import kr.ac.smu.vo.UserVo;

public class QueryUserAction extends Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String email = request.getParameter("email");		
		
		UserDao dao = new UserDao();
		UserVo vo =dao.getUser(email);
		
		response.setContentType( "application/json;charset=utf-8" );
		PrintWriter out = response.getWriter();

		if(vo==null){
			out.print("{ \"result\" :  \"not exist\" }");

		}else{
			out.print("{ \"result\" : \"exist\"}" ); 
		}
		
	}

}
