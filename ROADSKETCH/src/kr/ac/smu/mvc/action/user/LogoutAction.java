package kr.ac.smu.mvc.action.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;

public class LogoutAction extends Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String mobile= request.getParameter("mobile");
		
		HttpSession session= request.getSession(false);
		session.removeAttribute("session");
		session.invalidate();
		
		request.setAttribute("mobile",mobile);
		HttpUtil.redirect(request, response, "/roadsketch/main");
	}
}
